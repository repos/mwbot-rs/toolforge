## 5.8.0 / 2024-10-09
* Add `WikiPool::connect_to_slice()` to connect directly to a slice.
* Use the docsrs cfg option
* Add categories to Cargo.toml (suggested by lib.rs)
* Bump MSRV to 1.74.

## 5.7.0 / 2024-01-05
* Add `WikiPool::list_dbnames()` and `WikiPool::list_domains()`.
* Bump MSRV to 1.71.

## 5.6.0 / 2023-10-29
* Add support for connecting to ToolsDB.
* Stabilize `WikiPool`, a wrapper around `mysql_async::Pool`, that manages
  connections to wiki databases. It automatically routes connection requests
  for specific databases to their correct backend servers, aware of which
  databases share a server.
* Update `rust-ini` to 0.20.
* Bump MSRV to 1.70.

## 5.5.1 / 2023-07-30
* [UNSTABLE] Update `mysql_async` to 0.32.

## 5.5.0 / 2023-01-19
* `toolforge-tunnel` changes:
  * Have CI auto-publish binaries.
  * Use `clap` for argument parsing.
  * Gracefully handle `ctrl+c` to close connections.
  * Automatically re-open connections if the process quits for whatever reason.
* `Cluster` now implements `Copy` and `Clone`.
* Set `inactive_connection_ttl=1` to re-enable connection reuse.
* Bump MSRV to 1.64.
* Improve crate-level documentation.
* [UNSTABLE] `WikiPool` changes:
  * Add a builder so connection options are immutable.
  * Optimize how individual connection pools are stored.
  * Support connecting to `centralauth`.
  * `WikiPool` now implements `Clone`.
  * Re-export the `mysql_async` crate entirely.
  * Support connecting via wiki domain.

## 5.4.0 / 2022-12-26
* Rename `ToolsDBCluster` to `Cluster`. The old name will continue to work for
  backwards-compatibility purposes.
* Add a command-line tool, `toolforge-tunnel` to spin up SSH tunnels for local
  development with wiki replicas. It is very basic for now, with improvements
  and changes expected.
* [UNSTABLE] Add `WikiPool`, a wrapper around `mysql_async::Pool`, that manages
  connections to wiki databases. It automatically routes connection requests
  for specific databases to their correct backend servers, aware of which
  databases share a server. The API is not finalized and no stability
  guarantees are offered. You must enable the `unstable-pool` feature to use
  this for now.

## 5.3.0 / 2022-12-23
* Add options to enable compliant connection pooling.

## 5.2.0 / 2022-10-11
* Fix typo in documentation, MySQL port is always 3306.
* Moved Git repository to [Wikimedia GitLab](https://gitlab.wikimedia.org/repos/mwbot-rs/toolforge).
* Bump MSRV to 1.60, switch to edition 2021.

## 5.1.0 / 2022-01-09
* Use `tools.<tool>@toolforge.org` email addresses
* Have generated rustdoc indicate what features must be enabled
* Simplify DBConnectionInfo formatting code

## 5.0.0 / 2021-11-22
* No functional changes from 5.0.0-alpha.2

## 5.0.0-alpha.2 / 2021-10-10
* Have docs.rs build for all features

## 5.0.0-alpha.1 / 2021-10-10
* Bump major version to 5 to be in sync with python-toolforge
* Use local hostnames for connection_info! if local key is set in
  replica.my.cnf
* Hide documentation for `get_db_connection_info` to indicate it is prviate
  despite being public for use in a macro

## 0.3.1 / 2021-09-18
* Switch back to `dirs` crate, maintained once again
* Upgrade to `rust-ini` 0.17

## 0.3.0 / 2021-06-19
* No changes from 0.3.0-beta.1

## 0.3.0-beta.1 / 2021-02-01
* Switch to new Wiki Replicas multi-instance architecture

## 0.2.2 / 2021-01-23
* Switch to maintained `dirs-next` crate

## 0.2.1 / 2021-01-23
* Treat replica.my.cnf as ini, not toml

## 0.2.0 / 2020-09-15
* Add "mysql" feature for getting connection information to Wiki Replicas
* toolforge::user_agent! now returns `&'static str` to allow use in constant
  expressions

## 0.1.1 / 2020-08-28
* Add `toolforge.is_on_toolforge()`

## 0.1.0 / 2020-08-28
* Initial release
