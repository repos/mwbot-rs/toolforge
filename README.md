toolforge
============
[![crates.io](https://img.shields.io/crates/v/toolforge.svg)](https://crates.io/crates/toolforge)
[![docs.rs](https://docs.rs/toolforge/badge.svg)](https://docs.rs/toolforge)
[![pipeline status](https://gitlab.wikimedia.org/repos/mwbot-rs/toolforge/badges/main/pipeline.svg)](https://gitlab.wikimedia.org/repos/mwbot-rs/toolforge/-/commits/main)

The `toolforge` crate provides helper functions for various tasks on [Toolforge](https://toolforge.org/).

See [wikitech](https://wikitech.wikimedia.org/wiki/User:Legoktm/toolforge_library) for
the full documentation.

## License
toolforge is (C) 2013, 2017, 2020-2021 Kunal Mehta, released under the GPL v3 or
any later version, see COPYING for details.
