/*
Copyright (C) 2020 Kunal Mehta <legoktm@debian.org>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

use std::io;
use thiserror::Error as ThisError;

#[derive(ThisError, Debug)]
pub enum Error {
    #[error("The `{0}` file does not exist, maybe you aren't running this code on Toolforge?")]
    NotToolforge(String),
    #[error("Unable to read replica.my.cnf")]
    CantReadMyCnf(#[from] io::Error),
    #[error("Unable to parse replica.my.cnf")]
    CantParseMyCnf(#[from] ini::ParseError),
    #[cfg(feature = "wikipool")]
    #[error("MySQL error: {0}")]
    MysqlError(#[from] mysql_async::Error),
    #[cfg(feature = "wikipool")]
    #[error("Unknown database: {0}")]
    UnknownDatabase(String),
    #[cfg(feature = "wikipool")]
    #[error("Unknown domain: {0}")]
    UnknownDomain(String),
}
